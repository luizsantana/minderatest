//
//  ImagesTableViewCell.h
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagesTableViewCell : UITableViewCell

-(void) reloadData:(NSDictionary *) image1 andImage2:(NSDictionary *) image2;

@end
