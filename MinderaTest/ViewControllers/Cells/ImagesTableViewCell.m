//
//  ImagesTableViewCell.m
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import "ImagesTableViewCell.h"

#import "GetSizesRequest.h"

#import "UIImageView+Haneke.h"

@interface ImagesTableViewCell ()

@property(nonatomic, weak) IBOutlet UIImageView *imageView1;
@property(nonatomic, weak) IBOutlet UIImageView *imageView2;

@end

@implementation ImagesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadData:(NSDictionary *) image1 andImage2:(NSDictionary *) image2 {
    
    if (image1) {
        
        [GetSizesRequest getSizesPhotoID:[image1 objectForKey:@"id"] success:^(id responseData) {
            
            NSString *photoURLString;
            for (NSDictionary * photoSize in [[responseData objectForKey:@"sizes"] objectForKey:@"size"]) {
                if ([[photoSize valueForKey:@"label"] isEqualToString:@"Large"]) {
                    photoURLString = [photoSize valueForKey:@"source"];
                    break;
                }
            }
            
            [self.imageView1 hnk_setImageFromURL:[NSURL URLWithString:photoURLString]];
            
        } failure:^(NSError *error) {
            
        }];
    }
    
    if (image2) {
        
        [GetSizesRequest getSizesPhotoID:[image2 objectForKey:@"id"] success:^(id responseData) {
            
            NSString *photoURLString;
            for (NSDictionary * photoSize in [[responseData objectForKey:@"sizes"] objectForKey:@"size"]) {
                if ([[photoSize valueForKey:@"label"] isEqualToString:@"Large"]) {
                    photoURLString = [photoSize valueForKey:@"source"];
                    break;
                }
            }
            
            [self.imageView2 hnk_setImageFromURL:[NSURL URLWithString:photoURLString]];
            
        } failure:^(NSError *error) {
            
        }];
    }
}

@end
