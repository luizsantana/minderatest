//
//  ViewController.m
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 24/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import "ViewController.h"

#import "MinderaTest-Bridging-Header.h"

#import "MinderaTest-Swift.h"

#import "SearchRequest.h"

#import "UIScrollView+SVInfiniteScrolling.h"

#import "ImagesTableViewCell.h"

@interface ViewController ()  <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *imagesList;

@property(nonatomic) NSInteger page;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //[self.tableView registerClass:[ImagesTableViewCell class] forCellReuseIdentifier:@"ImageCell"];
    // Do any additional setup after loading the view, typically from a nib.
    self.imagesList = [NSMutableArray new];
    
    self.page = 1;
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.tableView triggerInfiniteScrolling];
    
    [SearchRequest searchPage:self.page success:^(id responseData) {
        
        id photos = [responseData objectForKey:@"photos"];
        [self.imagesList addObjectsFromArray:[photos objectForKey:@"photo"]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             [self. tableView reloadData];
        });
        
    } failure:^(NSError *error) {
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                       message:[error localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        
        self.page++;
        
        [SearchRequest searchPage:self.page success:^(id responseData) {
            
            id photos = [responseData objectForKey:@"photos"];
            [self.imagesList addObjectsFromArray:[photos objectForKey:@"photo"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView.infiniteScrollingView stopAnimating];
                [self.tableView reloadData];
            });

        } failure:^(NSError *error) {
            
            [self.tableView.infiniteScrollingView stopAnimating];
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Atenção"
                                                                           message:[error localizedDescription]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.imagesList count] > 0) {
        
        return ([self.imagesList count]/2);
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ImageCell";
    ImagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *image1 = [self.imagesList objectAtIndex:(indexPath.row * 2)];
    NSDictionary *image2;
    
    if ([self.imagesList count] > ((indexPath.row * 2) + 1)) {
        
        image2 = [self.imagesList objectAtIndex:indexPath.row + 1];
    }
    
    [cell reloadData:image1 andImage2:image2];
    
    return cell;
}

@end
