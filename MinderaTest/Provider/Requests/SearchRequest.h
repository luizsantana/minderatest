//
//  SearchRequest.h
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchRequest : NSObject

+ (void) searchPage:(NSInteger) page success:(void (^) (id responseData)) success failure:(void (^) (NSError * error)) failure;

@end
