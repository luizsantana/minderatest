//
//  SearchRequest.m
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 28/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import "SearchRequest.h"

#import "MinderaTest-Swift.h"

@implementation SearchRequest

+ (void) searchPage:(NSInteger) page success:(void (^) (id responseData)) success failure:(void (^) (NSError * error)) failure {
    
    BaseRequester *baseRequester = [[BaseRequester alloc] initWithPathToCertificate:[[NSBundle mainBundle] pathForResource:@"certificado" ofType:@"der"]  isPinning:YES];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@method=flickr.photos.search&api_key=%@&tags=kitten&page=%i&per_page=20&format=json&nojsoncallback=1", @"https://api.flickr.com/services/rest/?", @"f9cc014fa76b098f9e82f1c288379ea1", page]]];
    
    [baseRequester requestWith:request response:^(id _Nullable responseData, NSHTTPURLResponse * _Nullable response, NSError * _Nullable error) {
    
        if (error) {
            
            failure(error);
            return;
        }
        
        success(responseData);
    }];
}

@end
