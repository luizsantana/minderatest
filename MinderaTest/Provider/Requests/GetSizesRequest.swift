//
//  GetSizesRequest.swift
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 26/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

import Foundation

class GetSizesRequest: BaseRequest {
    
    init(photoId: String? = "") {
        self.photoId = photoId!
    }
    
    var photoId: String
    
    var endpoint: String {
        return String.init(format:"method=flickr.photos.getSizes&api_key=%@&photo_id=%@&format=json&nojsoncallback=1", "f9cc014fa76b098f9e82f1c288379ea1", photoId)
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var body:[String:Any] {
        return [:]
    }
    
    var hasSecurity: Bool {
        return true
    }
    
}
