//
//  HTTPMethod.swift
//  Connector-Example
//
//  Created by Luiz Sant'Ana on 02/11/17.
//  Copyright © 2017 Santander Brasil. All rights reserved.
//

import Foundation

@objc public enum HTTPMethod: Int {
    case options
    case get
    case head
    case post
    case put
    case patch
    case delete
    case trace
    case connect
    
    func toString() -> String {
        switch self {
        case .options: return "OPTIONS"
        case .get:     return "GET"
        case .head:    return "HEAD"
        case .post:    return "POST"
        case .put:     return "PUT"
        case .patch:   return "PATCH"
        case .delete:  return "DELETE"
        case .trace:   return "TRACE"
        case .connect: return "CONNECT"
        }
    }
}
