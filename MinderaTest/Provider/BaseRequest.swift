//
//  BaseRequest.swift
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 26/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

import Foundation

@objc public protocol BaseRequest {
    
    var endpoint: String {get}
    var method: HTTPMethod {get}
    @objc optional var body: [String : Any] {get}
    @objc optional var headers: [String : String] {get}
    @objc optional var timeoutInterval: TimeInterval {get}
    @objc optional var cachePolicy: URLRequest.CachePolicy {get}
}

public extension BaseRequest {
    
    func asURLRequestBase() -> URLRequest {
        
        let baseURL = "https://api.flickr.com/services/rest/?"
        
        let url = URL(string: "\(String(describing: baseURL))\(self.endpoint)")
        
        var request = URLRequest(url: url!)
        
        let timeoutInterval = 60.0
        
        let cachePolicy = URLRequest.CachePolicy.useProtocolCachePolicy
        
        request.timeoutInterval = timeoutInterval
        request.cachePolicy = cachePolicy
        
        request.httpMethod = self.method.toString()
        
        if let headers = self.headers {
            for header in headers {
                request.addValue(header.value, forHTTPHeaderField: header.key)
            }
        }
        
        return request
    }
    
    func asURLRequest() -> URLRequest {
        
        var request = asURLRequestBase()
        
        if let body = self.body {
            request.httpBody = (body as? Data)
        }
        return request
    }
}
