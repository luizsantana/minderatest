//
//  BaseRequester.swift
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 26/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

import Foundation

import Security

class BaseRequester: NSObject, URLSessionDelegate {
    
    // --------------------------------
    // MARK: - Initializers
    // --------------------------------
    
    init(pathToCertificate: String? = nil, isPinning: Bool = false) {
        self.pathToCertificate = pathToCertificate
    }
    
    // --------------------------------
    // MARK: - Properties
    // --------------------------------
    
    var session: URLSession!
    var pathToCertificate: String?
    
    // --------------------------------
    // MARK: - Request
    // --------------------------------
    
    func request(with urlRequest: URLRequest, response:@escaping (_ response: Any?, _ urlResponse: HTTPURLResponse?, _ error: NSError? ) -> Void) {
        
        let urlSessionConfiguration = URLSessionConfiguration.default
        self.session = URLSession(configuration: urlSessionConfiguration, delegate: self, delegateQueue: nil)
        
        self.session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            if (error != nil) {

                if (urlResponse != nil) {
                    
                    response(nil, (urlResponse as! HTTPURLResponse), error! as NSError)
                    
                    return
                }
                
                response(nil, nil, error! as NSError)
                
                return
            }
            
            guard let data = data else {
                let error = NSError.init(domain: "BaseRequester", code: -1, userInfo:nil)
                
                response(nil, (urlResponse as! HTTPURLResponse), error)
                return
            }
            
            do {
                if let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                    response(jsonResult, (urlResponse as! HTTPURLResponse), nil)
                    return
                } else if let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSArray {
                    response(jsonResult, (urlResponse as! HTTPURLResponse), nil)
                    return
                }
            } catch {

                response(nil, (urlResponse as! HTTPURLResponse), error as NSError)
                return
            }
            
            if let stringResult = NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue) {
                response(stringResult, (urlResponse as! HTTPURLResponse), nil)
                return
            }
            
            response(data, (urlResponse as! HTTPURLResponse), nil)
            
            }.resume()
    }
    
    // --------------------------------
    // MARK: - Pinning
    // --------------------------------
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if ((self.pathToCertificate != nil) && (self.pathToCertificate != "")) {
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
                if let serverTrust = challenge.protectionSpace.serverTrust, let pathToCertificate = self.pathToCertificate {
                    
                    if trustCertificate(serverTrust: serverTrust, certificatePath: pathToCertificate) {
                        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: serverTrust))
                        return
                    }
                }
            }
        } else if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: serverTrust))
            return
        }
        
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
    
    private func trustCertificate(serverTrust: SecTrust?, certificatePath: String) -> Bool {
        guard serverTrust != nil else { return false }
        
        let certificateNSURL = NSURL(fileURLWithPath: certificatePath)
        let certName = ((certificatePath as NSString).deletingPathExtension as NSString).lastPathComponent
        let certExtension = certificateNSURL.pathExtension
        
        let certBundle = Bundle.main.path(forResource: certName, ofType: certExtension)
        
        do {
            let certData = try Data(contentsOf: URL(fileURLWithPath: certBundle!))
            guard let cert = SecCertificateCreateWithData(nil, certData as CFData) else { return false }
            var certs = [SecCertificate]()
            certs.append(cert)
            
            SecTrustSetAnchorCertificates(serverTrust!, certs as NSArray)
            
            var result = SecTrustResultType(rawValue: 0)!
            SecTrustEvaluate(serverTrust!, &result)
            
            return (result == SecTrustResultType.unspecified) || (result == SecTrustResultType.proceed)
        } catch {

        }
        return false
    }
}
