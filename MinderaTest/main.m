//
//  main.m
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 24/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
