//
//  AppDelegate.h
//  MinderaTest
//
//  Created by Luiz Carlos Sant'Ana Junior on 24/2/18.
//  Copyright © 2018 Luiz Sant'Ana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

